# Workshop Ansible

## Pour commencer

### Git

Afin de simuler un environment de travail similaire à ce que vous allez trouver en production, nous utiliserons Git comme outil de travail. Vous conserverez ainsi une copie du laboratoire dans votre propre compte Git et pourrai y référer dans le futur. Ce même repository sera nécessaire pour la portion Ansible Tower du Workshop. Dans le présent workshop, nous utiliserons GitLab.


#### Étapes de configuration de votre compte GitLab

- À partir du serveur Linux qui vous est assigné, afficher la clé publique de votre usager Pod.
```
[podX@server ~]$ cat ~/.ssh/id_rsa.pub
ssh-rsa [...] Pfy0Zp2XVvVWPHtKX1yVc9AQpqpxyZfebVB1In [...]
```

- Dans GitLab, cliquez sur votre avatar en haut à gauche

- Sélectionner le menu "Settings"

- Cliquez sur "SSH Keys" dans la barre de menu de gauche

- Copiez-coller la clé publique que vous avez affiché dans le terminal précédemment dans le champ "Key"

- Indiquer le nom AnsibleWorkshop dans le champ "Title"

**Même si nous effacons tous les compte usagers sur le serveur Linux à la fin du Workshop, nous vous recommendons de supprimer cette clé SSH de votre profile à la fin du Workshop.**


#### Étapes pour faire un clone du repository

Afin que vous ayez accès à vos travaux à la suite des Workshop, vous allez faire une copie (Fork) du projet GitLab principal dans votre propre compte GitLab.

- Sur GitLab, aller sur la page principale de ce projet 

- Cliquer sur le bouton "Fork" pour créer une copie sur votre compte GitLab personnel. Selectionner votre compte. 

- Cliquer sur le bouton "Clone"

- Copier le lien dans la zone "Clone with SSH"

- Sur le pod qui vous est assigné, faire le "Clone" du projet

```
[podX@server ~]$ git clone git@gitlab.com:[VOTRE USAGER GITLAB]/ansible-workshop.git
Cloning into 'ansible-workshop'...
remote: Enumerating objects: 70, done.
remote: Counting objects: 100% (70/70), done.
remote: Compressing objects: 100% (58/58), done.
remote: Total 70 (delta 4), reused 0 (delta 0)
Receiving objects: 100% (70/70), 196.62 KiB | 0 bytes/s, done.
Resolving deltas: 100% (4/4), done.
```

- Configurer ensuite Git avec les informations de votre compte

```
git config --global user.email "VOTRE EMAIL"
git config --global user.name "VOTRE USAGER GITLAB"
```

#### Lab 1 - Tester la synchronisation avec votre Projet

A partir du serveur qui vous est assigné, vous allez créer un fichier, créer un "Commit" et pousser les changements dans votre Projet GitLab. 

Les mêmes étapes de cette section seront à faire à la fin de TOUS les laboratoires que vous ferez pendant le Workshop. 

- Naviguer vers le répertoire ansible-workshop/debutant/lab1-git-setup

```
[podX@server ~]$ cd ansible-workshop/debutant/lab1-git-setup/
```

- Créer un fichier nommé avec votre nom.prenom

Exemple:
```
[podX@server lab1-git-setup]$ touch martin.ouimet
```

- Ajouter les changements à votre Commit

```
[podX@server lab1-git-setup]$ git add --all
```

- Créer le Commit avec un message

```
[podX@server lab1-git-setup]$ git commit -m "Lab1"
[master 293e6d7] Lab1
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 debutant/lab1-git-setup/martin.ouimet
```

- Pousser les changements dans votre Projet GitLab

```
[podX@server lab1-git-setup]$ git push
Vous verrez un message Warning que vous pouvez ignorer dans ce laboratoire.

Counting objects: 7, done.
Delta compression using up to 2 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 392 bytes | 0 bytes/s, done.
Total 4 (delta 2), reused 0 (delta 0)
To git@gitlab.com:Ouimet/ansible-workshop.git
   47aba84..293e6d7  master -> master
```

- Aller maintenant sur votre page GitLab et faites un Refresh de la page

Naviguer les fichiers de votre projet à partir de GitLab jusqu'à 

debutant / lab1-git-setup

Vous devriez voir le nouveau fichier que vous avez créé. 

## Générer les fichiers inventaire

Naviguer au répertoire principal du projet.
```
cd ~/ansible-workshop
```

Exécuter le playbook generate-inventory.yaml afin de générer les fichiers inventaire pour votre environment. 
```
ansible-playbook generate-inventory.yaml
```

Le playbook vous demandera le numéro du Pod. Il est important d'inscrire le numéro du Pod qui vous a été assigné. 

Suite à l'exécution du playbook, ajouter les fichiers à votre repository Git afin de le mettre à jour. 

```
git add --all
git commit -m "added inventory"
git push
```

Faites de même à la fin de chaque laboratoire. 

## Félicitations ! 

Vous êtes maintenant pret pour commencer le Workshop Ansible !
